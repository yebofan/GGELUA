#include "gge.h"

static int LUA_malloc(lua_State *L)
{
    size_t size = luaL_checkinteger(L,1);
    GGE_Mem* ud = (GGE_Mem*)lua_newuserdata(L, sizeof(GGE_Mem));

    if (size>0)
		ud->ptr = (char*)SDL_malloc(size);
    else
        ud->ptr = 0;

    ud->size = size;
    luaL_setmetatable(L, "SDL_mem");
    return 1;
}

static int LUA_calloc(lua_State *L)
{
    size_t nmemb = luaL_checkinteger(L,1);
    size_t size = luaL_checkinteger(L,2);
    if (nmemb>0 && size>0)
    {
		GGE_Mem* ud = (GGE_Mem*)lua_newuserdata(L, sizeof(GGE_Mem));
		ud->ptr = (char*)SDL_calloc(nmemb,size);
		ud->size = nmemb*size;
		luaL_setmetatable(L, "SDL_mem");
        return 1;
    }
    return 0;
}

static int LUA_realloc(lua_State* L)
{
    GGE_Mem* ud = (GGE_Mem*)luaL_checkudata(L, 1, "SDL_mem");
    size_t size = luaL_checkinteger(L, 2);
    ud->ptr = SDL_realloc(ud->ptr, size);
    return 0;
}

static int LUA_free(lua_State *L)
{
    GGE_Mem* ud = (GGE_Mem*)luaL_checkudata(L, 1, "SDL_mem");
    if (ud->ptr)
    {
        SDL_free(ud->ptr);
        ud->ptr = 0;
    }
    return 0;
}

static int LUA_getsize(lua_State* L)
{
	GGE_Mem* ud = (GGE_Mem*)luaL_checkudata(L, 1, "SDL_mem");
    lua_pushinteger(L, ud->size);
	return 1;
}

static int LUA_getptr(lua_State* L)
{
	GGE_Mem* ud = (GGE_Mem*)luaL_checkudata(L, 1, "SDL_mem");
    int offset = (int)luaL_optinteger(L, 2, 0);
    if (!ud->ptr)
        return 0;
    lua_pushlightuserdata(L, ud->ptr + offset);
	return 1;
}

static const luaL_Reg mem_funcs[] = {
    { "realloc", LUA_realloc},
    { "getsize", LUA_getsize},
    { "getptr", LUA_getptr},
	{ "__gc", LUA_free}
};

static const luaL_Reg sdl_funcs[] = {
    { "malloc", LUA_malloc},
    { "calloc", LUA_calloc},
	{ NULL, NULL}
};

int bind_stdinc(lua_State* L)
{
	luaL_newmetatable(L, "SDL_mem");
	luaL_setfuncs(L, mem_funcs, 0);
	lua_pushvalue(L, -1);//ָ���Լ�
	lua_setfield(L, -2, "__index");
	lua_pop(L, 1);

    luaL_setfuncs(L, sdl_funcs, 0);
    return 0;
}