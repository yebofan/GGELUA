--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-05-03 14:19:49
--]]

local _ENV = require("GGE界面")

GUI按钮 = class("GUI按钮",GUI控件)
GUI按钮._type = 4
function GUI按钮:初始化(_,x,y,w,h)
    self._btn  = 1
    self.宽度  = w or 0
    self.高度  = h or 0
    self._sprs = setmetatable({}, {__index=function (t,i)--当t[?]不存在时，返回t[1] 
        return rawget(t, i) or rawget(t, 1)
    end})
end

function GUI按钮:_更新(dt)
    GUI控件._更新(self,dt)
    if self._spr and self._spr.更新 then
        self._spr:更新(dt)
    end
end

function GUI按钮:_显示(...)
    local _x,_y = self:取坐标()
    窗口:置区域(_x,_y,self.宽度,self.高度)
        if self._spr then
            self._spr:显示(_x,_y)
        end
    窗口:置区域()
    GUI控件._显示(self,...)
end

function GUI按钮:置禁止(v)
    if self.是否禁止 ~= v then
        self.是否禁止 = v==true
        self._btn = 0
        self._spr = self._sprs[0]
    end
    return self
end

function GUI按钮:置禁止精灵(v)
    self._sprs[0] = v
    if self._btn==0 then self._spr = v end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI按钮:置正常精灵(v)
    self._sprs[1] = v
    if self._btn==1 then self._spr = v end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI按钮:置按下精灵(v)
    self._sprs[2] = v
    if self._btn==2 then self._spr = v end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI按钮:置经过精灵(v)
    self._sprs[3] = v
    if self._btn==3 then self._spr = v end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI按钮:检查点(x,y)
    return self._spr and self._spr:检查点(x,y)
end

function GUI按钮:检查透明(x,y)
    return self._spr and self._spr:取透明(x,y)~= 0
end

function GUI按钮:置可见(v,s)
    GUI控件.置可见(self,v,s)
    if v and not self.是否禁止 then
        local _,x,y = 窗口:取鼠标状态()
        if self:检查透明(x,y) then
            self._btn = 3;self._spr = self._sprs[3]
        else
            self._btn = 1;self._spr = self._sprs[1]
        end
    end
    return self
end

function GUI按钮:置禁止(v)
    self.是否禁止 = v==true
    if self.是否禁止 then
        self._btn = 0;self._spr = self._sprs[0]
    else
        self._btn = 1;self._spr = self._sprs[1]
    end
    return self
end

function GUI按钮:取状态()
    if self._btn == 1 then
        return '正常'
    elseif self._btn == 2 then
        return '按下'
    elseif self._btn == 3 then
        return '经过'
    end
    return '禁止'
end

function GUI按钮:_消息事件(msg)
    if self.是否禁止 then
        return 
    end
    GUI控件._消息事件(self,msg)

    for _,v in ipairs(msg.鼠标) do
        if v.type==SDL.鼠标_按下 then
            if v.button==SDL.BUTTON_LEFT then
                if self:检查透明(v.x,v.y) then
                    v.type = nil
                    self._btn = 2;self._spr = self._sprs[2]
                    msg.按钮按下 = self
                    local x,y = self:取坐标()
                    self:发送消息("左键按下",x,y,msg)
                end
            end
        elseif v.type==SDL.鼠标_弹起 then
            if v.button==SDL.BUTTON_LEFT  then
                if self:检查透明(v.x,v.y) then
                    if self._btn==2 then
                        v.type = nil
                        self._btn = 3;self._spr = self._sprs[3]
                        msg.按钮弹起 = self--已经按下
                        local x,y = self:取坐标()
                        self:发送消息("左键弹起",x,y,msg)
                    else
                        msg.鼠标弹起 = self--没有按下
                    end
                elseif self._btn==2 then
                    self._btn = 1;self._spr = self._sprs[1]
                end
            end
        elseif v.type==SDL.鼠标_移动 then
            if v.state==0 then
                if self:检查透明(v.x,v.y) then
                    local x,y = self:取坐标()
                    self:发送消息("获得鼠标",x,y,msg)
                    v.x = -9999;v.y=-9999
                    if self._btn==1 then
                        self._btn = 3;self._spr = self._sprs[3]
                        msg.按钮经过 = self 
                    end
                elseif self._btn==3 then
                    self._btn = 1;self._spr = self._sprs[1]
                    msg.按钮经过 = false
                    self:发送消息("失去鼠标",v.x,v.y,msg)
                end
            end
        end
    end
    
end


GUI多选按钮 = class("GUI多选按钮",GUI按钮)
GUI多选按钮._type    = 6
function GUI多选按钮:初始化()
    
    self.是否选中 = false

    self._spra = setmetatable({}, {__index=function (t,i)--当t[?]不存在时，返回t[1] 
        return rawget(t, i) or rawget(t, 1)
    end})
    
    self._sprb = setmetatable({}, {__index=function (t,i)--当t[?]不存在时，返回t[1] 
        return rawget(t, i) or rawget(t, 1)
    end})
    self._sprs = self._spra
end

function GUI多选按钮:置禁止精灵(v)
    self._spra[0] = v
    if self._btn==0 then self._spr  = self._sprs[0] end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI多选按钮:置正常精灵(v)
    self._spra[1] = v
    if self._btn==1 then self._spr  = self._sprs[1] end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI多选按钮:置按下精灵(v)
    self._spra[2] = v
    if self._btn==2 then self._spr  = self._sprs[2] end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI多选按钮:置经过精灵(v)
    self._spra[3] = v
    if self._btn==3 then self._spr  = self._sprs[3] end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI多选按钮:置选中禁止精灵(v)
    self._sprb[0] = v
    if self._btn==0 then self._spr  = self._sprs[0] end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI多选按钮:置选中正常精灵(v)
    self._sprb[1] = v
    if self._btn==1 then self._spr  = self._sprs[1] end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI多选按钮:置选中按下精灵(v)
    self._sprb[2] = v
    if self._btn==2 then self._spr  = self._sprs[2] end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI多选按钮:置选中经过精灵(v)
    self._sprb[3] = v
    if self._btn==3 then self._spr  = self._sprs[3] end
    if v.宽度>self.宽度 or v.高度>self.高度 then
        self:置宽高(v.宽度,v.高度)
    end
    return self
end

function GUI多选按钮:置选中(v,msg,...)
    self.是否选中 = v==true
    self._sprs = v and self._sprb or self._spra
    self._spr  = self._sprs[self._btn]
    if msg~=false  and self.是否实例 then--防止循环
        self:发送消息("选中事件",self.是否选中,...)
    end
    return self
end

function GUI多选按钮:_消息事件(msg)
    GUI按钮._消息事件(self,msg)
    if msg.按钮弹起 == self then
        msg.按钮选中 = self
        self:置选中(not self.是否选中,true,msg)
    end
end


GUI单选按钮 = class("GUI单选按钮",GUI多选按钮)
GUI单选按钮._type = 5
-- function GUI单选按钮:初始化()
--     self._type = 5
-- end

function GUI单选按钮:置选中(v)
    if v==true and self.是否选中==false then
        for i,v in ipairs(self.父控件.子控件) do
            if v:取类型()=='单选按钮' then
                GUI多选按钮.置选中(v,v==self,false)
            end
        end
    end
    return self
end
