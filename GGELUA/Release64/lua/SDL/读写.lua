--[[
    @Author       : GGELUA
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-04-27 16:31:33
--]]

local _ENV = require("SDL")

local SDL读写 = class"SDL读写"

function SDL读写:SDL读写(file,mode)
    if type(file)=='string' then
        if type(mode)=='number' and #file==mode then
            self._str = file
            self._rw = RWFromStr(file,mode)
        else
            self._rw = RWFromFile(file,mode)
        end
    elseif type(file)=='userdata' then
        self._rw = RWFromMem(file)
    end
    if self._rw then
        -- body
    end
end

-- function SDL读写:__close()
    
-- end

function SDL读写:取对象()
    return self._rw
end

function SDL读写:取大小()
    return self._rw:RWsize()
end
RW_SEEK_SET = 0 --从头
RW_SEEK_CUR = 1 --当前
RW_SEEK_END = 2 --从尾

function SDL读写:置位置(offset,whence)
    return self._rw:RWseek(offset,whence or 0)
end

function SDL读写:取位置()
    return self._rw:RWtell()
end

function SDL读写:读取(size)
    return self._rw:RWread(size)
end

function SDL读写:写入(str)
    return self._rw:RWwrite(str)
end

SDL读写.seek  = SDL读写.置位置
SDL读写.tell  = SDL读写.取位置
SDL读写.read  = SDL读写.读取
SDL读写.write = SDL读写.写入
function SDL读写:读数据(bit,endian)
    if bit==8 then
        return self._rw:ReadU8()
    elseif bit==16 then
        return endian and self._rw:ReadBE16() or self._rw:ReadLE16()
    elseif bit==32 then
        return endian and self._rw:ReadBE32() or self._rw:ReadLE32()
    elseif bit==64 then
        return endian and self._rw:ReadBE64() or self._rw:ReadLE64()
    end
end

function SDL读写:写数据(data,bit,endian)
    if bit==8 then
        return self._rw:WriteU8(data)
    elseif bit==16 then
        return endian and self._rw:WriteBE16(data) or self._rw:WriteLE16(data)
    elseif bit==32 then
        return endian and self._rw:WriteBE32(data) or self._rw:WriteLE32(data)
    elseif bit==64 then
        return endian and self._rw:WriteBE64(data) or self._rw:WriteLE64(data)
    end
end

return SDL读写