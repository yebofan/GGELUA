--[[
    @Author       : GGELUA
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-04-30 08:32:37
--]]


local Astar = class("Astar")

function Astar:初始化(w,h,data)
    self._ud = require("gastar")(w,h,data)
    self._p = {}
    self.w = w
    self.h = h
end

function Astar:检查点(x,y)
    return self._ud:CheckPoint(x,y)
end

function Astar:寻路(x,y)
    return self._ud:GetPath(x,y)
end
return Astar